import React from 'react';
import { StyleSheet, View } from 'react-native';
import Todoapp from './screens/todoapp';
import store from './store';
import { Provider } from 'react-redux';
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Todoapp />
        </View>
      </Provider>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
