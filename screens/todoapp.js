import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, FlatList, Text, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IconV2 from 'react-native-vector-icons/AntDesign';
import { Card, Title } from 'react-native-paper';
import { connect } from 'react-redux';
import { addTodo, deleteTodo, checkedTodo } from '../actions/actions';
const todoapp = ({ todo_list, addTodo, deleteTodo }) => {



    const [task, setTask] = React.useState('');

    const handleAddTodo = () => {
        if (!task.trim()) {
            console.warn('Input can not be left blank');
            return;
        }
        addTodo(task)
        setTask('')
    }

    const handleDeleteTodo = (id) => {
        Alert.alert(
            'Warning',
            'Delete this task ?',
            [
              {text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel'},
              {text: 'YES', onPress: () => deleteTodo(id)},
            ]
          );

    }

    const show = id => {
        alert(`checkStatus: ` + id);
    }


    return (
        <View style={styles.container}>
            <Card.Content>
                <Title style={{ fontSize: 18 }}>Reduxを使用したToDoアプリ</Title>
            </Card.Content>
            <View style={styles.taskWrap}>
                <TextInput underlineColorAndroid="transparent" value={task} name={task}
                    onChangeText={task => setTask(task)} placeholder='Write some task here' style={styles.taskInput} />
                <TouchableOpacity onPress={handleAddTodo}><Icon size={20} name='feather-alt' /></TouchableOpacity>
            </View>
            <FlatList
                data={todo_list}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => {
                    return (
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 8 }}>
                            {/* <CheckBox value={item.checkStatus} onPress={()=>handleCheckedTodo(item.id)}  /> */}
                            <Text key={item.id} style={{ fontSize: 15, fontWeight: 'bold' }}>{item.id}.{item.task}</Text>
                            <IconV2 onPress={() => handleDeleteTodo(item.id)} size={18} style={{ color: '#eb0918' }} name='close' />
                        </View>

                    )
                }}
            />
        </View>
    )
}
const mapStateToProps = (state, ownProps) => {
    return {
        todo_list: state.todos.todo_list,
    }
}

const mapDispatchToProps = { addTodo, deleteTodo }

export default connect(mapStateToProps, mapDispatchToProps)(todoapp);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 40,
    },
    taskWrap: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    taskInput: {
        borderBottomColor: '#ddd',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        marginRight: 10,
        textAlign: 'center'
    }
});
